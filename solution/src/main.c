#include "stdlib.h"
#include <stdio.h>

#include "bmp_data.h"
#include "image.h"

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_ERROR = 1
};

long get_padding(uint64_t width) {
    long n = (long)width * 3;
    if (n % 4 == 0) {
        n = 0;
    } else {
        n = 4 - n % 4;
    }
    return n;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header;
    fread(&header, sizeof (struct bmp_header), 1, in);
    if (header.bfType != 19778) {
        img->height = 0;
        img->width = 0;
        img->data = NULL;
        return READ_ERROR;
    }
    img->height = header.biHeight;
    img->width = header.biWidth;
    if (fseek(in, header.bOffBits, SEEK_SET) != 0) {
        perror("error fseek");
        return READ_ERROR;
    }
    long n = get_padding(img->width);
    img->data = malloc(sizeof (struct pixel) * img->width * img->height);
    for (int i = 0; i < img->height; ++i) {
        for (int j = 0; j < img->width; ++j) {
            fread(&img->data[i * img->width + j], sizeof (struct pixel), 1, in);
        }
        if (fseek(in, n, SEEK_CUR) != 0) {
            perror("error fseek");
            return READ_ERROR;
        }
    }
    return READ_OK;
}

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR = 1
    /* коды других ошибок  */
};

enum write_status to_bmp( FILE* out, struct image const* img ) {
    int n = (int) get_padding(img->width);
    struct bmp_header header = {
        .bfType = 19778,
        .biHeight = img->height,
        .biWidth = img->width,
        .bOffBits = sizeof (struct bmp_header),
        .bfileSize = sizeof (struct bmp_header) + (img->width * img->height) * 3 + img->height * n,
        .bfReserved = 0,
        .biSize = 40,
        .biBitCount = 24,
        .biPlanes = 1,
        .biCompression = 0,
        .biSizeImage = (img->width * img->height) * 3 + img->height * n,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };

    fwrite(&header, sizeof(struct bmp_header), 1, out);
    for (int i = 0; i < img->height; ++i) {
        for (int j = 0; j < img->width; ++j) {
            if (fwrite(&img->data[i * img->width + j], sizeof (struct pixel), 1, out) == 0) {
                perror("error fwrite");
                return WRITE_ERROR;
            }
        }
        for (int j = 0; j < n; ++j) {
            fputc(0, out);
        }
    }
    return WRITE_OK;
}
/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct image const source ) {
    struct image image = {
            .width = source.height,
            .height = source.width,
            .data = malloc(sizeof(struct pixel) * source.width * source.height)
    };
    int n = 0;
    for (int i = 0; i < (int)source.width; ++i) {
        for (int j = (int)source.height - 1; j >= 0; --j) {
            image.data[n++] = source.data[j * source.width + i];
        }
    }
    return image;
}

void copy(struct image* source, struct image* dest) {
    if (source != NULL && dest != NULL) {
        dest->width = source->width;
        dest->height = source->height;
        dest->data = malloc(sizeof(struct pixel) * dest->width * dest->height);
        for (int i = 0; i < dest->width * dest->height; ++i) {
            dest->data[i] = source->data[i];
        }
    }
}

int main( int argc, char** argv ) {
    struct image image;
    if (argc != 4) {
        perror("number of arguments must be 4");
        return 2;
    }
    char *inputImage = argv[argc - 3];
    char *resultImage = argv[argc - 2];
    long angle = strtol(argv[argc - 1], 0, 10);
    FILE* file = fopen(inputImage, "rb");
    if (file == NULL) {
        perror("file not found");
        return 1;
    }
    enum read_status rStatus = from_bmp(file, &image);
    if (rStatus == READ_ERROR) {
        return 3;
    }
    fclose(file);
    struct image result;
    struct image temp;
    copy(&image, &result);
    long n = 4 - ((angle + 360)/90) % 4;
    for (int i = 0; i < n; ++i) {
        temp = rotate(result);
        free(result.data);
        copy(&temp, &result);
        free(temp.data);
    }
    file = fopen(resultImage, "wb");
    if (file == NULL) {
        perror("file can't be create");
        free(result.data);
        return 1;
    }
    to_bmp(file, &result);
    free(image.data);
    free(result.data);
    fclose(file);
    return 0;
}
